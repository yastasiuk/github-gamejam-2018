﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour {
	
	// player controls
	[Range(0.0f, 10.0f)] // create a slider in the edit0or and set limits on moveSpeed
	public float moveSpeed = 3f;

	protected Rigidbody2D rb2d;
	protected Vector2 targetVelocity;
    protected Vector2 velocity;
	
	void OnEnable() {
        rb2d = GetComponent<Rigidbody2D> ();
    }

	// Use this for initialization
	void Start () {
		
	}

	protected void ComputeVelocity() {
		Vector2 move = Vector2.zero;

        move.x = Input.GetAxis ("Horizontal");
        move.y = Input.GetAxis ("Vertical");
        targetVelocity = move * moveSpeed;
	}
	
	// Update is called once per frame
	void Update () {
		ComputeVelocity();   
	}

	void FixedUpdate() {
		Movement(targetVelocity, true); 
	}

    void SetColor(Color color) {
        print("Setting new color:" + color.ToString()   );
    }

	void Movement(Vector2 move, bool yMovement) {
        float distance = move.magnitude;

        // if (distance > minMoveDistance) 
        // {
        //     int count = rb2d.Cast (move, contactFilter, hitBuffer, distance + shellRadius);
        //     hitBufferList.Clear ();
        //     for (int i = 0; i < count; i++) {
        //         hitBufferList.Add (hitBuffer [i]);
        //     }

        //     for (int i = 0; i < hitBufferList.Count; i++) 
        //     {
        //         Vector2 currentNormal = hitBufferList [i].normal;
        //         if (currentNormal.y > minGroundNormalY) 
        //         {
        //             grounded = true;
        //             if (yMovement) 
        //             {
        //                 groundNormal = currentNormal;
        //                 currentNormal.x = 0;
        //             }
        //         }

        //         float projection = Vector2.Dot (velocity, currentNormal);
        //         if (projection < 0) 
        //         {
        //             velocity = velocity - projection * currentNormal;
        //         }

        //         float modifiedDistance = hitBufferList [i].distance - shellRadius;
        //         distance = modifiedDistance < distance ? modifiedDistance : distance;
        //     }


        // }
		
        rb2d.position = rb2d.position + move.normalized * distance / 10;
    }
}
