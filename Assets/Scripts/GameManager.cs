﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	// static reference to game manager so can be called from other scripts directly (not just through gameobject component)
	public static GameManager gm;

	private GameObject player;
	private List<PlayerBlock> playersBlocks;
	private string _spawnAnchorName;
	private Vector3 defaultPlayerPosition = Vector3.zero;

	// set things up here
	void Awake () {
		// setup reference to game manager
		if (gm == null) {
			gm = this.GetComponent<GameManager>();
		}
		
		DontDestroyOnLoad(gm);
		// setup all the variables, the UI, and provide errors if things not setup properly.
		setupDefaults();
		removeNonUsedTags();
	}
    
	void OnEnable() {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnSceneLoaded(Scene scene,  LoadSceneMode mode) {
		setupDefaults();
    }

	void setupDefaults () {
		GameObject spawnAnchor = GameObject.Find(_spawnAnchorName);
		if (player == null) {
            player = GameObject.FindGameObjectWithTag("Player");
		}
        
        if (player==null) {
			Debug.LogError("Player not found in Game Manager");
		}
		if (playersBlocks != null) {
			player.GetComponent<PlayerController>().SetPlayerBlocks(playersBlocks);
		}
		if (spawnAnchor != null) {
			player.GetComponent<PlayerController>().SetPlayersPosition(spawnAnchor.transform.position);
		}
				
	}

	void removeNonUsedTags() {
		foreach (GameObject obj in GameObject.FindGameObjectsWithTag("EditorOnly")) {
			obj.SetActive(false);
		}
	}

	public void publicLevelCompete(string nextLevel, string spawnAnchorName) {
		// save the current player prefs before moving to the next level
		// PlayerPrefManager.SavePlayerState(score,highscore,lives);
		GetAndSavePlayerBlocks();
		_spawnAnchorName = spawnAnchorName;
		// use a coroutine to allow the player to get fanfare before moving to next level
		StartCoroutine(LoadNextLevel(nextLevel));
	}

	IEnumerator LoadNextLevel(string nextLevel) {
		yield return new WaitForSeconds(0.0f); 
		Application.LoadLevel (nextLevel);

		yield return new WaitForSeconds(5);
	}

	public void GetAndSavePlayerBlocks() {
		playersBlocks = player.GetComponent<PlayerController>().GetPlayerBlocks();
	}
	
}
