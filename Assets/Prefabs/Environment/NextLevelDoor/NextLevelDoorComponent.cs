﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class NextLevelDoorComponent : MonoBehaviour {

	public SceneField nextLevel;
	public string SpawnAnchorName;
	
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D collidedObj) {
		// If the player enters the trigger zone...
		if(collidedObj.tag == "Player") {
			print("Initializing transition to:" + nextLevel.SceneName);
			GameManager.gm.publicLevelCompete(nextLevel.SceneName, SpawnAnchorName);
		}
	}
}
