﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DyeProperies : MonoBehaviour {
	// Use this for initialization
	protected Color spriteColor;

	void Start () {
        SpriteRenderer rend = GetComponent<SpriteRenderer>();
		spriteColor = rend.color;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D collidedObj) {
		// If the player enters the trigger zone...
		if(collidedObj.tag == "PlayerSector") {
			Destroy(transform.root.gameObject);
		}
	}

	public Color GetColor() {
		return spriteColor;
	}
}
