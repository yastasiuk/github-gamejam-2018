﻿using UnityEngine;

public class PlayerBlock {
    public Color Color { get; set; }
    public Vector3 Position { get; set; }
    public PlayerBlock(Color color, Vector3 position) {
        Color = color;
        Position = position;
    }
}