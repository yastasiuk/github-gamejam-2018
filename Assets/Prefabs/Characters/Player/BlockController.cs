﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockController : MonoBehaviour {
	protected SpriteRenderer render;

	
	void OnEnable() {
		render = GetComponent<SpriteRenderer>();
	}

	void Start () {
		SetDefault();
	}
	
	void Update () {
		
	}
	
	void OnTriggerEnter2D (Collider2D collidedObj) {
		print("Collided with:" + collidedObj.tag);
		if(collidedObj.tag == "Dye") {
			SetColor(collidedObj.gameObject.GetComponent<DyeProperies>().GetColor());
		}
	}

	void SetDefault() {
		this.gameObject.transform.localRotation = Quaternion.identity;
		this.gameObject.transform.localScale = new Vector3(3.5f, 3.5f, 0.0f);
	}

	public void SetBlockState(PlayerBlock playerBlock) {
		SetColor(playerBlock.Color);
		SetPosition(playerBlock.Position);
	}

	void SetColor(Color newColor) {
		render.color = newColor;
	}

	void SetPosition(Vector3 newPosition) {
		this.gameObject.transform.localPosition = newPosition;
	}

	public Color GetColor() {
		return render.color;
	}

	public Vector3 GetPosition() {
		return this.gameObject.transform.localPosition;
	}
}
