﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	
	[Range(1, 4)]
	public int numberOfParts = 1;
	
	[Range(0.0f, 10.0f)]
	public float moveSpeed = 3f;

	// Just hardcoding for now, anyway we don"t need to add/remove number of PlayerSections
	// public GameObject TopLeft;
	// public GameObject TopRight;
	// public GameObject BottomLeft;
	// public GameObject BottomRight;
	public GameObject PlayerBlockPrefab;

	protected string prefabPath;
	public Color DefaultColor = Color.white;

	protected Rigidbody2D rb2d;
	protected Vector2 targetVelocity;

	protected List<PlayerBlock> _playerBlocks;
	protected List<GameObject> _playerBlockComponents = new List<GameObject>();
	
	// Unity methods
	void OnEnable() {
        rb2d = GetComponent<Rigidbody2D> ();
    }

	void Start () {
		if (_playerBlocks == null) {
			_playerBlocks =  new List<PlayerBlock>()
				{
					new PlayerBlock(DefaultColor, new Vector3(-0.55f, 0.0f, 0.0f)),
					new PlayerBlock(DefaultColor, new Vector3(0.55f, 0.0f, 0.0f)),
					new PlayerBlock(DefaultColor, new Vector3(-0.55f, -1.1f, 0.0f)),
					new PlayerBlock(DefaultColor, new Vector3(0.55f, -1.1f, 0.0f)),
				};
		}
		InitializePlayerBlocks();
	}
	
	void Update () {
		ComputeVelocity();
	}

	void FixedUpdate() {
		Movement(targetVelocity, true); 
	}

	// Custom methods
	protected void ComputeVelocity() {
		Vector2 move = Vector2.zero;

        move.x = Input.GetAxis ("Horizontal");
        move.y = Input.GetAxis ("Vertical");
        targetVelocity = move * moveSpeed;
	}

	void Movement(Vector2 move, bool yMovement) {
        float distance = move.magnitude;
        rb2d.position = rb2d.position + move.normalized * distance / 10;
    }

	public List<PlayerBlock> GetPlayerBlocks() {
		var playerBlocks = new List<PlayerBlock>();
		foreach (var component in _playerBlockComponents) {
			playerBlocks.Add(new PlayerBlock(component.GetComponent<BlockController>().GetColor(), component.GetComponent<BlockController>().GetPosition()));
		}
		return playerBlocks;
	}

	// TODO: remove or get use of it(currently we just hardcoded number of blocks & location)
	void addInnerParts(int numberOfParts) {
		Vector3 PlayerSize = GetComponent<BoxCollider2D>().bounds.size;
	}
	
	public void SetPlayerBlocks(List<PlayerBlock> gmPlayerBlocks) {
		_playerBlocks = gmPlayerBlocks;
	}

	public void SetPlayersPosition(Vector3 newPosition) {
		this.gameObject.transform.position = newPosition;
	}

	void InitializePlayerBlocks() {
		foreach (var playerBlock in _playerBlocks) {
			initializePlayerBlock(playerBlock);
		}
	}

	void initializePlayerBlock(PlayerBlock playerBlock) {
		GameObject pbObject = Instantiate(PlayerBlockPrefab);// as GameObject; 
		pbObject.transform.parent = this.gameObject.transform;
		pbObject.GetComponent<BlockController>().SetBlockState(playerBlock);
		_playerBlockComponents.Add(pbObject);
	}
}
